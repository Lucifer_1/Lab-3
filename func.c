#include <stdio.h>
#include <math.h>
#include "func.h"


int read_args(float* a, float* b, var* n)
{
    int rc = 0;

    printf("Enter the range like (a b) and the number of repeats\n");
    if ((scanf("%f %f %u",a, b, n))!= 3)
        rc = ERR_READ_ARGS;

    return rc;
}

int calc_tg(float a, float b, var n)
{
    int rc = 0;
    var i = 0;
    print_char(CHARS);

    for (float j = a; i < n; i++ ,j+= step(a, b, n))
    {
        print_out(i, j);
    }
    return rc;
}

void calc_tg_do_while(float a, float b, var n)
{

    var i;
    float j = a;
    print_char(CHARS);
    do
    {
        print_out(i, j);
        i++;
        j+= step(a, b, n);

    }while(i < n);

}

int calc_tg_while(FILE* file, float a, float b, var n)
{
    int rc = 0;
    if (file != NULL)
    {
        var i = 0;

        float j = step(a, b, n);

        fprintf(file, "N |\t\t     X \t\t   f(x)\n");
        while(i < n)
        {
            fprintf(file, "%d | \t %f |\t %f  \n", i, j, tg_x(j));
            i++;
            j+=step(a, b, n);
        }
    }
    else
        rc = ERR_OPEN_FILE;
    fclose(file);
    return rc;
}


float step(float a, float b, var n)
{
    if (a*b<=0)
        return (a = (fabs(a) + fabs(b))/(n-1));
    else
        return(a = (fabs(b)-fabs(a))/(n-1));

}

float tg_x(float x)
{
    float form;

    form = tanf(x) * sqrt(x);

    return form;
}

void print_char(int n)
{
    for (int i = 0; i < n; i++)
        printf("_");
    printf("\n");
    printf("N |\t     X |\t  f(x)|\n");
    for (int i = 0; i < n; i++)
        printf("_");
    printf("\n");
}



void print_out(var i, float j)
{
    printf("%d | \t %f |\t %f  \n", i, j, tg_x(j));
}
