#ifndef _FUNC_H
#define _FUNC_H

//variables
typedef unsigned int var;

//definitions

#define ERR_INVALID_START_ARGS 1
#define ERR_OPEN_FILE 2
#define ERR_READ_ARGS 3
#define FUNC_READ_ARGS_BROKE 4
#define CHARS 35

//functions

int read_args(float *, float*, var*);
int calc_tg(float, float, var);
float tg_x(float);
void print_char(int);
void print_out(var, float);
float step(float, float, var);
int calc_tg_while(FILE*,float, float, var);
void calc_tg_do_while(float, float, var);


#endif
