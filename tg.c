#include <stdio.h>
#include <stdbool.h>
#include "func.h"

int main(int argc, char *argv[])
{
    setbuf(stdout, NULL);
    int rc = 0;
    var n;
    float a, b;
    if (argc == 2)
    {
        FILE *file = fopen(argv[1], "r+w");

        if (file != NULL)
        {
            if ((read_args(&a, &b, &n)) == rc)
            {
                // printf("%f %f %d \n", a, b, n);
                calc_tg(a, b, n);
                if ((calc_tg_while(file, a, b, n)) == rc)
                {
                    calc_tg_do_while(a, b, n);
                }
            }
            else
                rc = FUNC_READ_ARGS_BROKE;
        }
        else
            rc = ERR_OPEN_FILE;

        fclose(file);
    }
    else
        rc = ERR_INVALID_START_ARGS;

    printf("Mistakes = %d\n",rc);
    return rc;
}
