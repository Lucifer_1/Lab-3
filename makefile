CFLAGS := -std=c99 -Wall -Werror

main.exe: tg.o func.o
	gcc $(CFLAGS) -o $@ $^

tg.o : tg.c
	gcc $(CFLAGS) -c $^

func.o : func.c func.h
	gcc $(CFLAGS) -c $<

clean :
	rm *.o *.i
